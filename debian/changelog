ncmpc (0.33-1) UNRELEASED; urgency=medium

  * New upstream release.
    - Fix "CVE-2018-9240 (Closes: #894724)
    - Fix "segfault on bad connection" (Closes: #902699)
    - Fix "Defaults to non-policy-compliant configuration
      file" (Closes: #896059)
    - Add Build-depends on meson and python3-sphinx (dropped doxygen)
    - Add Build-depends libpcre
  * Update standards version to 4.2.1:
    - Update debhelper to compat 11
  * Switch from menu to XDG Desktop file
  * Build documentation in separated package ncmpc-doc
  * Update upstream Homepage

 -- Geoffroy Youri Berret <efrim@azylum.org>  Sun, 02 Dec 2018 15:46:49 +0100

ncmpc (0.27-1) unstable; urgency=medium

  * New upstream release.
    - Drop lirc.patch; included upstream.
      Remove dh_autoreconf which isn't needed anymore.
  * Update standards version to 4.1.1:
    - Update to debhelper version 10.
    - Drop build-deps on autotools-dev and dh-autoreconf which are no longer
      needed.

 -- Sebastian Harl <tokkee@debian.org>  Sun, 22 Oct 2017 14:25:02 +0200

ncmpc (0.25-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release
    - this release includes the autoreconf fixes
  * debian/patches/lirc.patch:
    - Adapt the configure script to detect the new lirc (Closes: #842326).
      (patch has been accepted upstream)

 -- Alec Leamas <leamas@nowhere.net>  Fri, 28 Oct 2016 07:05:23 +0200

ncmpc (0.24-1) unstable; urgency=medium

  * New upstream release.

 -- Sebastian Harl <tokkee@debian.org>  Tue, 15 Jul 2014 20:50:09 +0200

ncmpc (0.23-1) unstable; urgency=low

  * New upstream release.
  * debian/rules:
    - Removed work-around for leoslyrics; the scripts were removed upstream.

 -- Sebastian Harl <tokkee@debian.org>  Sat, 12 Jul 2014 19:30:57 +0200

ncmpc (0.21-1) unstable; urgency=low

  * New upstream release (Closes: #659815).
    - Fixes a crash on the lyrics screen; thanks to Jonathan Neuschäfer for
      reporting this (Closes: #638830).
    - Fixes linking with ld --as-needed; thanks to Matthias Klose for
      reporting this (Closes: #641612).
    - Fixes connection reporting when using $MPD_HOST; thanks to Daniel Kahn
      Gillmor for reporting this (Closes: #605710).
  * debian/control:
    - Build-depend on libmpdclient-dev (>= 2.5~); this is required for the new
      chat screen.
    - Updated to standards-version 3.9.5.
  * debian/rules:
    - Enable chat-screen feature.
    - Split build target into build-arch and build-indep.
    - Use dpkg-buildflags to get CPPFLAGS, CFLAGS, and LDFLAGS.
    - Do not install leoslyrics plugins; Leo's Lyrics's API seems to be done;
      thanks to Thorsten Sperber for reporting this (Closes: #654243).
  * debian/control, debian/rules:
    - Enable documentation and build-depend on doxygen.
  * debian/patches, debian/control:
    - Dropped bts589660-updated-manpage; merged upstream.
    - Dropped build-dependency on dpatch.
    - Dropped debian/README.source.
  * debian/source/format:
    - Switch to source format '3.0 (quilt)'; thanks to Jari Aalto for
      reporting this and providing initial patches (Closes: #668488).

 -- Sebastian Harl <tokkee@debian.org>  Sun, 27 Apr 2014 14:06:11 +0200

ncmpc (0.17-1) unstable; urgency=low

  * New upstream release.
    - Fixed range selection in "shuffle" command; thanks to Michael Marineau
      for reporting this (Closes: #585823).
  * debian/patches:
    - Added bts589660-updated-manpage -- updated obsolete parts of the
      manpage; thanks to Martin Hradil for reporting this (Closes: #589660).
  * debian/control, debian/copyright:
    - Updated standards-version to 3.9.1 -- include the complete BSD license
      in debian/copyright rather than referencing the file in /u/s/common-
      licenses.

 -- Sebastian Harl <tokkee@debian.org>  Fri, 30 Jul 2010 14:49:31 +0200

ncmpc (0.16.1-1) unstable; urgency=low

  * New upstream release.
  * debian/patches:
    - Removed include_config_h.dpatch -- included upstream.
    - Removed manpage_typos.dpatch -- included upstream.

 -- Sebastian Harl <tokkee@debian.org>  Tue, 19 Jan 2010 21:22:21 +0100

ncmpc (0.16-1) unstable; urgency=low

  * New upstream release:
    - Fixed accuracy of "Database updated" message; thanks to Joey Hess for
      reporting this (Closes: #548266).
    - Update volume display without delay; thanks to Joey Hess for reporting
      this (Closes: #551271).
    - Execute lyrics plugins according to their lexical sorting order; thanks
      to Jonathan Neuschäfer for reporting this (Closes: #553063).
  * debian/control:
    - Build-depend on libmpdclient-dev, required by the new upstream release.
    - Build-depend on dpatch.
    - Let all packages depend on ${misc:Depends}.
  * debian/rules:
    - Run 'make check' unless the nocheck option has been specified in
      DEB_BUILD_OPTIONS.
  * debian/patches:
    - Added include_config_h.dpatch -- this patch adds some missing includes.
    - Added manpage_typos.dpatch -- this patch fixes some typos in the
      manpage.
  * Added debian/README.source:
    - The file includes a pointer to /usr/share/doc/dpatch/README.source.gz.

 -- Sebastian Harl <tokkee@debian.org>  Sun, 27 Dec 2009 23:43:36 +0100

ncmpc (0.15-1) unstable; urgency=low

  * New upstream release:
    - Fixed NULL pointer dereference when not yet connected; thanks to Joey
      Hess for reporting this (Closes: #540415).
  * debian/control:
    - Updated Standards-Version to 3.8.3 - no changes.

 -- Sebastian Harl <tokkee@debian.org>  Thu, 08 Oct 2009 22:05:01 +0200

ncmpc (0.14-1) unstable; urgency=low

  * Re-upload to unstable - Lenny has been released.
  * debian/control:
    - Updated Standards-Version to 3.8.1 (no changes).
    - Updated version of build-dependency debhelper to >= 7.
  * debian/compat:
    - Updated from deprecated version 4 to the now recommended version 7.
  * debian/rules:
    - Use dh_prep instead of the deprecated dh_clean -k.
    - Pass CFLAGS as arguments to configure instead of setting them in the
      environment - this is the recommended way.

 -- Sebastian Harl <tokkee@debian.org>  Sat, 06 Jun 2009 20:19:58 +0200

ncmpc (0.13-1) experimental; urgency=low

  * New upstream release:
    - Made status line clock display configurable (Closes: #510392).
  * debian/changelog:
    - Upstream (partially) switched to mentioning "The Music Player Daemon
      Project" as copyright holder instead of listing single persons for each
      file. debian/changelog now refers to the AUTHORS file for a list of team
      members.
    - Use © instead of (C) to make lintian happy.
  * Added binary package "ncmpc-lyrics". This package includes the lyrics
    plugins, some of which depend on python or ruby. By splitting them into a
    separate package, we avoid any kind of dependency on python or ruby in the
    "ncmpc" package which now suggests ncmpc-lyrics instead of python, ruby.

 -- Sebastian Harl <sh@tokkee.org>  Sun, 11 Jan 2009 21:36:39 +0100

ncmpc (0.12-1) experimental; urgency=low

  * New upstream release:
    - Added support for wide characters (Closes: #395407).
    - Fixed a segfault when working with empty or removed directories
      (Closes: #471761).
    - Added command "locate" ('G') which locates a song in the database
      browser; thanks to Hagen Fuchs for the original patch (Closes: #496450).
    - Fixed connection handling (Closes: #497359).
    - Fixed handling of the "timedisplay-type" config option; thanks to Stefan
      Bender for the original patch (Closes: #497917).
  * Uploading to experimental because of the Lenny freeze.
  * debian/control:
    - Added Homepage and Vcs-{Git,Browser} fields.
    - Added versioned build dependency on dpkg-dev (>= 1.14.6) to support the
      new fields.
    - Suggest (instead of recommend) mpd - server and client may run on
      different host, so not having both packages installed is not unusual
      (Closes: #447781).
    - Updated to Standards-Version 3.8.0.
    - Build-depend on liblircclient-dev - this is required for lirc support.
    - Suggest python and ruby - these are required by the lyrics scripts.
    - Updated package description.
  * debian/watch:
    - Query sf.net instead of the old upstream website.
    - Use uversionmangle to mangle "_alpha" and "_beta".
  * debian/rules:
    - Do not try to install upstream ChangeLog and TODO files - they no longer
      exist.
    - Do not ignore "make distclean" errors.
    - -I/usr/include/ncursesw is no longer required - it's now handled by the
      upstream configure script.
    - Explicitly enable all features when running the configure script.
  * debian/menu:
    - Use section "Applications" instead of "Apps".

 -- Sebastian Harl <sh@tokkee.org>  Sun, 07 Dec 2008 17:34:07 +0100

ncmpc (0.11.1+svn-r3965-2) unstable; urgency=low

  * New maintainer (Closes: #388425).
  * Rewrote debian/rules to not use cdbs.
  * Updated standards version to 3.7.2.

 -- Sebastian Harl <sh@tokkee.org>  Fri, 22 Sep 2006 21:33:38 +0200

ncmpc (0.11.1+svn-r3965-1) unstable; urgency=low

  * New upstream version

    + includes the patch to implement Tab completion in the
      libcursesw support part of wreadln.c (closes: #357660)

    + includes 07-fix-utf8-strlen.diff, which had been contributed by
      Dmitry Baryshkov. It is therefore removed from the Debian package

  * debian/copyright: list all copyright holders of any file. Based on
    latest discussions.
    + debian/rules: do not install superfluous AUTHORS file anymore

  * debian/dirs: remove "/usr/bin", it is automatically created

  * Upload sponsored by Norbert Tretkowski <nobse@debian.org>

 -- René van Bevern <rvb@progn.org>  Tue, 28 Mar 2006 20:20:41 +0200

ncmpc (0.11.1+svn-r3896-1) unstable; urgency=low

  * New upstream release
    + removed the patches 05-russian-translation.diff,
      06-russian-typo.diff, they are not needed any more

    + added patch 07-fix-utf8-strlen.diff by Dmitry Baryshkov to correctly
      calculate length of UTF-8 strings (closes: #353772)

  * debian/rules: remove --with-ncursesw from configure options, since
    upstream has set it to default

  * Upload sponsored by Norbert Tretkowski <nobse@debian.org>

 -- René van Bevern <rvb@progn.org>  Tue, 21 Feb 2006 10:58:43 +0100

ncmpc (0.11.1+svn-r3362-1) unstable; urgency=low

  * Fresh SVN snapshot
    + number versions by subversion revisions rather than dates

  * 05-russian-translation.diff is back, it had not been included
    upstream, but made it into my svn work tree accidently

  * include 06-russian-typo.diff from Stepan Golosunov (closes: #328907)

 -- René van Bevern <rvb@pro-linux.de>  Wed, 14 Dec 2005 17:48:39 +0100

ncmpc (0.11.1+svn-20050916-1) unstable; urgency=low

  * New upstream release (SVN snapshot)
    + includes fixes for wide character display: (Closes: #326074)
      - debian/rules: build --with-ncursesw
                      add /usr/include/libncursesw to include path
      - debian/control: Build-Depend on libncursesw5-dev

  * Following patches are removed because they are included upstream:
    + 01-startup-segfault.diff
    + 03-xterm-title-crash.diff
    + 04-german-translation.diff
    + 05-russian-translation.diff

  * debian/patches/02-manpage-confdir.diff is party applied upstream and
    has been adapted.

  * added watch file

  * Upload sponsored by Norbert Tretkowski <nobse@debian.org>

 -- René van Bevern <rvb@pro-linux.de>  Fri, 16 Sep 2005 19:52:55 +0200

ncmpc (0.11.1-9) unstable; urgency=low

  * debian/patches/05-russian-translation.diff: patch by
    Stepan Golosunov <stepan@golosunov.pp.ru> (closes: #326070)
    + actual encoding of ru.po is koi8-r, not iso-8859-1

  * debian/copyright: updated adress of the Free Software Foundation

  * debian/control: updated package description with full sentences and
    (hopefully) without grammar errors

  * Upload sponsored by Norbert Tretkowski <nobse@debian.org>

 -- René van Bevern <rvb@pro-linux.de>  Sun,  4 Sep 2005 16:50:17 +0200

ncmpc (0.11.1-8) unstable; urgency=low

  * rebuild for menu transition
  * update to Standards-Version 3.6.2
  * switch to cdbs and split my patches into seperate files again
  * more specific debian/copyright

  * Upload sponsored by Norbert Tretkowski <nobse@debian.org>

 -- René van Bevern <rvb@pro-linux.de>  Thu, 11 Aug 2005 02:13:45 +0200

ncmpc (0.11.1-7) unstable; urgency=low

  * Closes: #312815: FTBFS: lacks a binary-arch target

 -- René van Bevern <rvb@pro-linux.de>  Fri, 10 Jun 2005 11:34:27 +0200

ncmpc (0.11.1-6) unstable; urgency=low

  * Use the upstream build system, adapt debian/rules instead
  * loosened the build dependecies (no automake and autoconf)
  * added homepage URL to description
  * backport of svn against upstream crash #360
    see: http://www.musicpd.org/mantis/view.php?id=360
  * dropped dpatch in favor of Darcs (README.Packaging)

 -- René van Bevern <rvb@pro-linux.de>  Fri,  3 Jun 2005 13:53:27 +0200

ncmpc (0.11.1-5) unstable; urgency=low

  * Closes: #306163: compile with search and clock feature
  * Closes: #306260: Looks for global config file in /usr/etc instead of /etc

  * Upload sponsored by Norbert Tretkowski <nobse@debian.org>

 -- Rene van Bevern (RvB) <rvb@pro-linux.de>  Mon, 25 Apr 2005 16:27:14 +0200

ncmpc (0.11.1-4) unstable; urgency=low

  * don't install a ChangeLog.gz through dh_installdocs because
    it is installed by dh_installchangelogs
  * Closes: #304246: doesn't work when not specifying password

  * Upload sponsored by Norbert Tretkowski <nobse@debian.org>

 -- Rene van Bevern (RvB) <rvb@pro-linux.de>  Tue, 12 Apr 2005 13:32:30 +0200

ncmpc (0.11.1-3) unstable; urgency=low

  * Closes: #267942: ITP: ncmpc -- An ncurses client for Music Player Daemon (MPD)
  * fix location of user configuration file in manual page
  * fix a segmentation fault when password is neither given as a command line
    option nor as an environment variable
  * little corrections of the german localization
  * use dpatch
  * add menu support

  * Upload sponsored by Norbert Tretkowski <nobse@debian.org>

 -- Rene van Bevern (RvB) <rvb@pro-linux.de>  Sun, 10 Apr 2005 11:42:43 +0200

ncmpc (0.11.1-2) unreleased; urgency=low

  * get rid of empty /usr/sbin and /usr/share/ncmpc in package

 -- Rene van Bevern (RvB) <rvb@pro-linux.de>  Fri,  8 Apr 2005 18:46:35 +0200

ncmpc (0.11.1-1) unreleased; urgency=low

  * Initial Release.

 -- Rene van Bevern (RvB) <rvb@pro-linux.de>  Wed, 30 Mar 2005 18:35:27 +0200
