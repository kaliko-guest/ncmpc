Source: ncmpc
Section: sound
Priority: optional
Maintainer: Sebastian Harl <tokkee@debian.org>
Build-Depends: debhelper-compat (= 11),
        meson (>= 0.47),
        libncursesw5-dev,
        libboost-dev,
        libmpdclient-dev (>= 2.9),
        liblircclient-dev,
        pkg-config,
        libpcre++-dev,
# for doc
        python3-sphinx
Standards-Version: 4.2.1
Homepage: https://www.musicpd.org/clients/ncmpc/
Vcs-Git: git://git.tokkee.org/pkg-ncmpc.git
Vcs-Browser: http://git.tokkee.org/?p=pkg-ncmpc.git

Package: ncmpc
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: mpd, ncmpc-lyrics
Enhances: mpd
Provides: mpd-client
Description: ncurses-based audio player
 This package contains a text-mode client for MPD, the Music Player Daemon.
 Its goal is to provide a keyboard oriented and consistent interface to MPD,
 without wasting resources.
 .
 Features:
  - full Unicode and wide character support
  - music database browser, database search, media library
  - audio output configuration
  - lyrics
  - LIRC support
  - customizable key bindings and colors
  - tiny memory footprint, smaller than any other interactive MPD client

Package: ncmpc-lyrics
Architecture: all
Depends: ncmpc, ruby, ${misc:Depends}
Description: ncurses-based audio player (lyrics plugins)
 ncmpc is a text-mode client for MPD, the Music Player Daemon. Its goal is to
 provide a keyboard oriented and consistent interface to MPD, without wasting
 resources.
 .
 This package contains plugins to download lyrics.

Package: ncmpc-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: ncurses-based audio player (lyrics plugins)
 ncmpc is a text-mode client for MPD, the Music Player Daemon. Its goal is to
 provide a keyboard oriented and consistent interface to MPD, without wasting
 resources.
 .
 This package contains the html documentation.
